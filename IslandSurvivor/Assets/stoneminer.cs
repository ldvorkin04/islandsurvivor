using UnityEngine;

public class StoneCutter : MonoBehaviour
{
    public GameObject metal;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "kirka")
        {
            Destroy(gameObject);
            Instantiate(metal, transform.position, Quaternion.identity); ;
        }
    }
}