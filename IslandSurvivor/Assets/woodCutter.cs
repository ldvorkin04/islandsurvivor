using UnityEngine;

public class AxeCollision : MonoBehaviour
{
    public GameObject wood;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "axe")
        {
            Destroy(gameObject);
            Instantiate(wood, transform.position, Quaternion.identity);
        }
    }
}