using UnityEngine;
using System.Collections.Generic;

public class ArchipelagoGenerator : MonoBehaviour
{
    // List of island models
    public GameObject[] islandModels;

    // Sequence of island types to generate
    public IslandType[] islandSequence;

    // Parameters for island generation
    public int islandCount = 100;
    public float islandSpacingMin = 50f;
    public float islandSpacingMax = 200f;
    public int archipelagoSize = 1000;

    // List of generated islands
    private List<GameObject> islands = new List<GameObject>();

    void Start()
    {
        GenerateArchipelago();
    }

    void GenerateArchipelago()
    {
        // Clear the list of islands
        islands.Clear();

        // Generate islands in the specified sequence
        for (int i = 0; i < islandCount; i++)
        {
            // Get the next island type in the sequence
            IslandType islandType = islandSequence[i % islandSequence.Length];

            // Get a random island model from the list
            GameObject islandModel = islandModels[Random.Range(0, islandModels.Length)];

            // Create a new instance of the island model
            GameObject island = Instantiate(islandModel);

            // Set the island's position and rotation
            island.transform.position = GetRandomPosition(islandSpacingMin, islandSpacingMax);


            // Add the island to the list
            islands.Add(island);
        }
    }

    Vector3 GetRandomPosition(float minSpacing, float maxSpacing)
    {
        // Generate a random position within the archipelago bounds
        float x = Random.Range(-archipelagoSize / 2, archipelagoSize / 2);
        float z = Random.Range(-archipelagoSize / 2, archipelagoSize / 2);
        return new Vector3(x, 0, z);
    }
}

public enum IslandType { Desert, Forest, Mountain, Beach };