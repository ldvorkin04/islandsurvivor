using UnityEngine;
using UnityEngine.UI;

public class CottonStalk : MonoBehaviour
{
    // The maximum strength of the cotton stalk
    public int maxStrength = 100;

    // The current strength of the cotton stalk
    public int currentStrength;

    // The knife object tag
    public string knifeTag = "Knife";

    // The object to replace the cotton stalk with when its strength reaches zero or below
    public GameObject replacementObject;

    // The health bar object to display the current strength of the cotton stalk
    public Slider healthBar;

    void Start()
    {
        // Set the initial strength of the cotton stalk
        currentStrength = maxStrength;

        // Update the health bar to display the initial strength
        UpdateHealthBar();
    }

    void Update()
    {
        // Check for collisions with knife objects
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1f, LayerMask.GetMask("Default"));
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.tag == knifeTag)
            {
                // Subtract 33 from the current strength of the cotton stalk
                currentStrength -= 15;

                // Update the health bar to display the new strength
                UpdateHealthBar();

                // If the strength is less than or equal to zero, replace the cotton stalk with the specified object
                if (currentStrength <= 0)
                {
                    Destroy(gameObject);
                    Instantiate(replacementObject, transform.position, transform.rotation);
                }
            }
        }
    }

    void UpdateHealthBar()
    {
        // Update the health bar to display the current strength of the cotton stalk
        healthBar.value = currentStrength;
    }
}