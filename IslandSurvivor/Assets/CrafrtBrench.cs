using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftBrench : MonoBehaviour
{ 
    [SerializeField] public GameObject Doska;
    [SerializeField] public GameObject Korma;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("WoodBench") && gameObject.CompareTag("WoodBench"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
            Instantiate(Doska, transform.position, Quaternion.identity);
        }
        if (collision.gameObject.CompareTag("Doska") && gameObject.CompareTag("Doska"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
            Instantiate(Korma, transform.position, Quaternion.identity);
        }
    }
}

